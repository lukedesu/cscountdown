package cscountdown;

/**
 * @author gianl
**/

public class ThreadedCountdown extends Thread{
    
    private int time;
    
    public ThreadedCountdown(int t){
        time = t;
    }
    
    @Override
    public void run(){
        System.out.println("[!] Connection expiring in " + time + " second(s)");
        
        // salvo il valore del tempo e sottraggo di uno ogni 1000 ms (1 secondo)
        int clock = time;
        int i;
        
        for(i = 0; i < clock; i++){
            
            // sta roba è troppo complicata? si
            // la semplificherò pur sacrificando una semplice funzione? no
            // ne vale la pena? assolutamente si.
            // hotel? trivago
            System.out.println("[*]" + (time == 1 ? (time + " second ") : (time + " seconds ")) + "left");
            time--;
            try {        
                
                /*  Nota
                    // TIL: netbeans indica che l'utilizzo di (Thread.)sleep all'interno di un loop non è consigliato
                    // Dopo qualche ricerca ho scoperto che è principalmente per evitare problemi di performance
                    // Ref. https://stackoverflow.com/questions/3535754/netbeans-java-new-hint-thread-sleep-called-in-loop
                */
                sleep(1000);
                
            } catch (InterruptedException ex) {
                System.out.print(ex);
            }
        }
    }
}
