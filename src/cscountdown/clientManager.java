package cscountdown;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 *
 * @author gianl
 */
public class clientManager {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Client client = new Client(InetAddress.getLocalHost(), 1234);
            client.newTimer();
        } catch (UnknownHostException e) {
            System.err.print(e);
        }
    }
    
}
