package cscountdown;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
/**
 *
 * @author gianl
 */
public class Server {
    
    private int _port;
    private ServerSocket _socket;
    private int serverTimeOut;
    private Socket socket;
    private BufferedReader clientGet;
    private BufferedWriter clientPost;
    private String receivedMsg;
    private String sentMsg;
    
    public Server(int port, int to){
        try {
            // Open the connection to the server
            _port = port;
            serverTimeOut = to*1000;
            _socket = new ServerSocket(_port);
            _socket.setSoTimeout(serverTimeOut);
            System.out.println("[*] Successfully started server");
        } catch (SocketTimeoutException e) {
            System.out.println("[*] Server Exception");
            System.err.print(e);
        } catch (ConnectException e) {
            System.err.print(e);
        } catch (SocketException e) {
            System.err.print(e);
        } catch (IOException e) {
            System.err.print(e);
        }
    }
    
    public void wait(int toClient){
        try {
            socket = _socket.accept();
            socket.setSoTimeout(toClient * 1000);
            System.out.println("[*] [" + socket + " ] Succesfully connected!");
            System.out.println("[*] Updated soTimeout");
        } catch (SocketException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.print(e);
        }
    }
    
    public void write(String msgToSend){
        try {
            clientPost = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            clientPost.write(msgToSend + "\n");
            clientPost.flush();
        } catch (IOException e) {
            System.err.print(e);
        }
    }
    
    public String read(){
        try {
            clientGet = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            
            receivedMsg = clientGet.readLine();
            return receivedMsg;
        } catch (IOException e) {
            System.err.print(e);
            return "-1";
        }
    }
    
    public void inviaTimeStamp(){
        receivedMsg = read();
        switch(receivedMsg){
            case "req_time" -> {
                sentMsg = Long.toString(System.currentTimeMillis());
                System.out.println("[*] Sending Timestamp");
            }
                         
            case "test" -> {
                sentMsg = "test";
                System.out.println("[*] Sending test message");
            }
            default -> sentMsg = "invalid";            
        }
        write(sentMsg);
    }
    
    public void kill(){
        try {
            // Kill the connection
            socket.close();
        } catch (IOException e) {
            System.err.print(e);
        }
    }
}
