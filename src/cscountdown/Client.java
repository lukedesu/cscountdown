package cscountdown;

/**
 *
 * @author gianl
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Timestamp;

public class Client {
    
    private InetAddress _connecAddress; // Connection Address
    private int _port; // Port for the connection
    
    private Socket socket;
    
    
    private BufferedReader inputReader;
    private BufferedReader brGet; // FROM server
    private BufferedWriter bwPost; // TO server 
    private String messageIn; // Message to be received
    private String messageOut;// Message to be sent
    
    public Client(InetAddress addr, int port){
        try {
            // Attempt to connect to server
            _connecAddress = addr;
            _port = port;
            socket = new Socket(_connecAddress, _port);
        } catch (UnknownHostException e) {
            System.err.println("[!] Connection Error: Host not found");
            System.err.print(e);
        } catch (IOException e) {
            System.out.println("[!] Connection Error: an unknown error occurred");
            System.err.print(e);
        }
    }
    
    public void newTimer(){
        ThreadedCountdown timer = new ThreadedCountdown(Integer.parseInt(read()));
        timer.start();
    }
    
    public void write(){
        try {
            
            inputReader = new BufferedReader(new InputStreamReader(System.in));
            bwPost = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            
            messageOut = inputReader.readLine();
                        
            bwPost.write(messageOut + "\n"); // NOTE: next time don't forget \n kty
            bwPost.flush();
            
        } catch (IOException e) {
            System.err.print(e);
        }
    }
    
    public void write(String messageOut){
        try {
            inputReader = new BufferedReader(new InputStreamReader(System.in));
            bwPost = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            
            // Send the message
            bwPost.write(messageOut + "\n");
            bwPost.flush();
        } catch (IOException e) {
            System.err.print(e);
        }
    }
    
    public String read(){
        try {
            brGet = new BufferedReader(new InputStreamReader(socket.getInputStream()));
 
            // Read the message
            messageIn = brGet.readLine();
            return messageIn;
        } catch (IOException e) {
            System.err.print(e);
            return "-1";
        }
    }
    
    public void requestTimestamp(){
        try {
            // Ask the server for the timestamp
            write("req_time");
            
            // get the message and stuff
            brGet = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            messageIn = brGet.readLine();
            switch(messageIn){
                case "invalid" -> System.err.println("[!] Server: Invalid request");
                    
                default -> {
                        Timestamp serverDate = new Timestamp(Long.parseLong(messageIn));
                        System.out.println("[*] Server clock: "+ serverDate);
                }
            }
        } catch (IOException e) {
            System.err.print(e);
        }
    }
    
    // haha funny class name go brr
    // you should kill(yourself) now!
    // kill(me)
    public void kill(){
        try {
            socket.close(); // stab stab stab
        } catch (IOException e) {
            System.err.print(e);
        }
    }
}
