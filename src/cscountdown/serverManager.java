package cscountdown;

/**
 *
 * @author gianl
 */
public class serverManager {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int to = 10;
        Server server = new Server(1234, 20);
        while(true){
            server.wait(to);
            server.write(String.valueOf(to));
        }
    }
    
}
